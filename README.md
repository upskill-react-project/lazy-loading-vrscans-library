# Lazy Loading VRScans Library

This is a React-based load-on-demand library that displays VRScans materials from a pre-defined REST API. It provides a user-friendly interface with filters, search functionality, and lazy-loading of materials. Additionally, it includes user accounts with authentication using Auth0, a user profile page, and shopping cart functionality with Stripe integration for checkout.


## Getting Started

To use this project, follow these steps:

### Prerequisites

-   Node.js installed on your system
-   Git for version control

### Installation

1.  Clone the repository to your local machine:
```
git clone https://gitlab.com/upskill-react-project/lazy-loading-vrscans-library.git
```

2. Change into the project directory:
```
cd lazy-loading-vrscans-library
```

3. Install the project dependencies:
```
npm install
```

### Configuration

Before running the application, make sure to configure the following:

-   Auth0: Set up your Auth0 credentials and configure them in the application.
-   Stripe: Configure your Stripe API keys for payment processing.

### Running the Application

To start the development server and run the application locally, use the following command:
```
npm start
```
This will start the development server, and the application will be available at `http://localhost:3000`.

### Running Tests

To run unit, integration, and end-to-end tests, you can use the following commands:

-   Unit tests:
```
npm test
```
- Integration tests:
```
npm run e2e
```
- Headless integration tests (run without a graphical interface):
```
npm run e2e:headless
```
### Deployment

This project is set up for Continuous Integration (CI) and Continuous Deployment (CD) using GitLab Pages. The deployment process is automated when changes are pushed to the `main` branch.
[Link to deployed version](https://lazy-loading-vrscans-library-lazy-loading-vrscan-a6a3ff98f69cef.gitlab.io/)

## Features

-   Load-on-demand display of VRScans materials from a REST API.
-   User authentication and account management with Auth0.
-   User profile page for editing user details.
-   Material filters with clear indication of selected filters.
-   Lazy loading of materials for improved performance.
-   Search functionality to filter materials by name.
-   Favourite materials feature for logged-in users.
-   Shopping cart functionality with Stripe integration for checkout.

## Issues

If you encounter any issues or have suggestions for improvement, please visit the [issue tracker](https://gitlab.com/upskill-react-project/lazy-loading-vrscans-library/-/issues) and create a new issue.

## Authors

-   Mihaila Krusharska
-   Anna Usanli