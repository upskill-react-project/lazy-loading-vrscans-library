import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import ShopContextProvider, { ShopContext } from './ShopContextProvider';


let localStorageMock = {};
global.localStorage = {
  getItem: (key) => localStorageMock[key],
  setItem: (key, value) => {
    localStorageMock[key] = value.toString();
  },
  clear: () => {
    localStorageMock = {};
  }
};

const TestComponent = () => (
  <ShopContext.Consumer>
    {(context) => (
      <>
        <div data-testid="cartItemsLength">{context.cartItems.length}</div>
        <button
          data-testid="addToCartButton"
          onClick={() => context.addToCart({ id: 1, price: 10 })}
        >
          Add to Cart
        </button>
        <button data-testid="removeFromCartButton" onClick={() => context.removeFromCart(1)}>
          Remove from Cart
        </button>
        <div data-testid="isItemInCart">{context.isItemInCart(1) ? 'true' : 'false'}</div>
        <div data-testid="getTotalCartAmount">{context.getTotalCartAmount()}</div>
        <button data-testid="clearCartButton" onClick={() => context.clearCart()}>
          Clear Cart
        </button>
      </>
    )}
  </ShopContext.Consumer>
);

describe('ShopContextProvider', () => {
  it('should render with initial cart items', () => {
   
    localStorage.setItem('cartItems', JSON.stringify([{ id: 1, price: 10 }]));

    const { getByTestId } = render(
      <ShopContextProvider>
        <TestComponent />
      </ShopContextProvider>
    );

    expect(getByTestId('cartItemsLength').textContent).toBe('1');
  });

  it('should add an item to the cart', () => {
    const { getByTestId } = render(
      <ShopContextProvider>
        <TestComponent />
      </ShopContextProvider>
    );

    fireEvent.click(getByTestId('addToCartButton'));
    expect(getByTestId('cartItemsLength').textContent).toBe('1');
    expect(getByTestId('isItemInCart').textContent).toBe('true');
    expect(getByTestId('getTotalCartAmount').textContent).toBe('10');
  });

  it('should remove an item from the cart', () => {
    const { getByTestId } = render(
      <ShopContextProvider>
        <TestComponent />
      </ShopContextProvider>
    );

    fireEvent.click(getByTestId('addToCartButton'));
    fireEvent.click(getByTestId('removeFromCartButton'));
    expect(getByTestId('cartItemsLength').textContent).toBe('0');
    expect(getByTestId('isItemInCart').textContent).toBe('false');
    expect(getByTestId('getTotalCartAmount').textContent).toBe('0');
  });

  it('should clear the cart', () => {
    const { getByTestId } = render(
      <ShopContextProvider>
        <TestComponent />
      </ShopContextProvider>
    );

    fireEvent.click(getByTestId('addToCartButton'));
    fireEvent.click(getByTestId('clearCartButton'));
    expect(getByTestId('cartItemsLength').textContent).toBe('0');
    expect(getByTestId('isItemInCart').textContent).toBe('false');
    expect(getByTestId('getTotalCartAmount').textContent).toBe('0');
  });
});
