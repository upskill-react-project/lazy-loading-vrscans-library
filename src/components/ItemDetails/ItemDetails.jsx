import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { getApi } from '../../api/api.calls';
import { endPoints } from '../../api/api.config';

const ItemDetails = () => {
  const { id } = useParams();
  //console.log('id:', id);

  const [vrscan, setVrscan] = useState([]);
  useEffect(() => {
    getApi(endPoints.vrscans, id)
      .then((res) => {
        setVrscan(res.data);
      })
      .catch((err) => console.log(err));
  }, [id]);

  //console.log('vrscan', vrscan);

  return (
    <div className="item-details">
      <img src={vrscan?.thumb} />
      {vrscan?.name}
    </div>
  );
};

export default ItemDetails;
