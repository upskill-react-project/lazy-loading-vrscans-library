import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import useAuthWithLocalStorage from '../../hooks/useAuthWithLocalStorage';
import {
  setColors,
  setMaterials,
  setTags,
  useFetchColorsQuery,
  useFetchMaterialsQuery,
  useFetchTagsQuery
} from '../../store';
import './LeftMenu.scss';
import LeftMenuItem from './LeftMenuItem';

function LeftMenu() {
  const { isAuthorised } = useAuthWithLocalStorage();

  const dispatch = useDispatch();

  const { tagsFilter, colorsFilter, materialsFilter } = useSelector((state) => {
    return {
      tagsFilter: state.filters.tags,
      colorsFilter: state.filters.colors,
      materialsFilter: state.filters.materials
    };
  });

  const {
    data: materials,
    error: materialsError,
    isFetching: isFetchingMaterials
  } = useFetchMaterialsQuery();

  const { data: colors, error: colorsError, isFetching: isFetchingColors } = useFetchColorsQuery();
  const { data: tags, error: tagsError, isFetching: isFetchingTags } = useFetchTagsQuery();

  const updateCheckedItems = (checkedItems, event) => {
    let newCheckedItems = [];
    const itemId = +event.target.name;
    if (event.target.checked === true) {
      // check
      newCheckedItems = [...checkedItems, itemId];
    } else {
      // uncheck
      newCheckedItems = checkedItems.filter(function (item) {
        return item !== itemId;
      });
    }
    return newCheckedItems;
  };

  const handleMaterials = (event) => {
    const newMaterials = updateCheckedItems(materialsFilter, event);
    dispatch(setMaterials(newMaterials));
    /*
    const newMenuFilters = { ...menuFilters, materials: newMaterials };
    setMenuFilters(newMenuFilters);
    */
  };

  const handleTags = (event) => {
    const newTags = updateCheckedItems(tagsFilter, event);
    dispatch(setTags(newTags));
    /*
    const newMenuFilters = { ...menuFilters, tags: newTags };
    setMenuFilters(newMenuFilters);
    */
  };

  const handleColors = (event) => {
    const newColors = updateCheckedItems(colorsFilter, event);
    dispatch(setColors(newColors));
    /*
    const newMenuFilters = { ...menuFilters, colors: newColors };
    setMenuFilters(newMenuFilters);
    */
  };

  return (
    <div className="left-menu">
      <div className="accordion" id="accordionPanelsStayOpenExample">
        {isAuthorised ? (
          <LeftMenuItem
            title="Materials"
            tag="Material"
            expanded={true}
            items={materials}
            isLoading={isFetchingMaterials}
            loadingError={materialsError}
            checkedItems={materialsFilter}
            handleChange={(event) => handleMaterials(event)}
          />
        ) : (
          <LeftMenuItem
            title="Materials"
            tag="Material"
            expanded={false}
            items={materials}
            isLoading={isFetchingMaterials}
            loadingError={materialsError}
            checkedItems={materialsFilter}
            handleChange={(event) => handleMaterials(event)}
          />
        )}

        <LeftMenuItem
          title="Tags"
          tag="Tag"
          expanded={true}
          items={tags}
          isLoading={isFetchingTags}
          loadingError={tagsError}
          checkedItems={tagsFilter}
          handleChange={(event) => handleTags(event)}
        />

        <LeftMenuItem
          title="Colors"
          tag="Color"
          expanded={true}
          items={colors}
          isLoading={isFetchingColors}
          loadingError={colorsError}
          checkedItems={colorsFilter}
          handleChange={(event) => handleColors(event)}
        />
      </div>
    </div>
  );
}

export default LeftMenu;
