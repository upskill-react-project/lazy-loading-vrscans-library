import React from 'react';
import useAuthWithLocalStorage from '../../hooks/useAuthWithLocalStorage';
import './LeftMenu.scss';

const LeftMenuItem = ({
  title,
  tag,
  expanded,
  items,
  isLoading,
  loadingError,
  checkedItems,
  handleChange
}) => {
  const { logIn, isAuthorised } = useAuthWithLocalStorage();

  const renderLoading = () => {
    return <div>Loading...</div>;
  };

  const renderError = () => {
    return <div>Error loading filters.</div>;
  };

  const renderItems = () => {
    if (!items?.length) return <></>;
    return (
      <ul className="menu-filters">
        {items?.map((item) => (
          <li key={item.id}>
            <input
              className="styled-checkbox"
              id={`styled-checkbox-${tag}${item.id}`}
              type="checkbox"
              name={item.id}
              checked={checkedItems.includes(item.id)}
              onChange={handleChange}
            />
            <label htmlFor={`styled-checkbox-${tag}${item.id}`}>
              {item.hex ? <i style={{ background: `${item.hex}` }}></i> : ''}{' '}
              <span>{item.name}</span>
            </label>
          </li>
        ))}
      </ul>
    );
  };

  return (
    <div className="accordion-item">
      <h2 className="accordion-header">
        {!isAuthorised && !expanded ? (
          <button
            className={`accordion-button ${expanded ? '' : 'collapsed'}`}
            type="button"
            data-bs-toggle="collapse"
            data-bs-target={`#panelsStayOpen-collapse${tag}`}
            aria-expanded={`${expanded ? 'false' : 'true'}`}
            aria-controls={`panelsStayOpen-collapse${tag}`}
            onClick={logIn}
          >
            {title} <i className="text-lowercase fw-normal">&nbsp;- you must be sign in</i>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              height="16"
              width="10"
              fill="white"
              viewBox="0 0 320 512"
            >
              <path d="M278.6 233.4c12.5 12.5 12.5 32.8 0 45.3l-160 160c-12.5 12.5-32.8 12.5-45.3 0s-12.5-32.8 0-45.3L210.7 256 73.4 118.6c-12.5-12.5-12.5-32.8 0-45.3s32.8-12.5 45.3 0l160 160z" />
            </svg>
          </button>
        ) : (
          <button
            className={`accordion-button ${expanded ? '' : 'collapsed'}`}
            type="button"
            data-bs-toggle="collapse"
            data-bs-target={`#panelsStayOpen-collapse${tag}`}
            aria-expanded={`${expanded ? 'false' : 'true'}`}
            aria-controls={`panelsStayOpen-collapse${tag}`}
          >
            {title}
            <svg
              xmlns="http://www.w3.org/2000/svg"
              height="16"
              width="10"
              fill="white"
              viewBox="0 0 320 512"
            >
              <path d="M278.6 233.4c12.5 12.5 12.5 32.8 0 45.3l-160 160c-12.5 12.5-32.8 12.5-45.3 0s-12.5-32.8 0-45.3L210.7 256 73.4 118.6c-12.5-12.5-12.5-32.8 0-45.3s32.8-12.5 45.3 0l160 160z" />
            </svg>
          </button>
        )}
      </h2>

      {expanded ? (
        <div
          id={`panelsStayOpen-collapse${tag}`}
          className={`accordion-collapse collapse  ${expanded ? 'show' : ''}`}
        >
          <div className="accordion-body">
            {(isLoading && renderLoading()) || (loadingError && renderError()) || renderItems()}
          </div>
        </div>
      ) : (
        <></>
      )}
    </div>
  );
};

export default LeftMenuItem;
