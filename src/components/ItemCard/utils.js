export const favoritesChecker = (favorites, id) => {
  if (favorites === undefined || favorites === null) {
    return false;
  }
  return favorites.some((item) => item.id === id);
};
