import { favoritesChecker } from './utils';

describe('favoritesChecker', () => {
  it('returns true when id is in the favorites', () => {
    const favorites = [{ id: 1 }, { id: 2 }, { id: 3 }];
    const idToCheck = 2;
    expect(favoritesChecker(favorites, idToCheck)).toBe(true);
  });

  it('returns false when favorites array is empty', () => {
    const favorites = [];
    const idToCheck = 1;
    expect(favoritesChecker(favorites, idToCheck)).toBe(false);
  });

  // Optional: Test for edge cases
  it('returns false when favorites is undefined or null', () => {
    expect(favoritesChecker(undefined, 1)).toBe(false);
    expect(favoritesChecker(null, 1)).toBe(false);
  });

  it('returns false when id is undefined or null', () => {
    const favorites = [{ id: 1 }, { id: 2 }, { id: 3 }];
    expect(favoritesChecker(favorites, undefined)).toBe(false);
    expect(favoritesChecker(favorites, null)).toBe(false);
  });
});
