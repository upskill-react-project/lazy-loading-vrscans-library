import React from 'react';
import { Link } from 'react-router-dom';
import './LeftMenuNoFiltersMobile.scss';

function LeftMenuMobile() {
  return (
    <div className="d-flex mobile-menu2">
      <div className="dropdown">
        <Link to="/shopping-cart" className="mm-button">
          <i>Your&nbsp;</i>Cart
        </Link>
      </div>
      <div className="dropdown">
        <Link to="/favorites" className="mm-button">
          Favorites<i>&nbsp;Page</i>
        </Link>
      </div>
      <div className="dropdown">
        <Link to="/profile" className="mm-button">
          Profile<i>&nbsp;Page</i>
        </Link>
      </div>
      <div className="dropdown">
        <Link to="/" className="mm-button">
          Library
        </Link>
      </div>
    </div>
  );
}

export default LeftMenuMobile;
