import React from 'react';
import { render } from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom';
import LeftMenuMobile from './LeftMenuNoFiltersMobile';
import '@testing-library/jest-dom';

describe('LeftMenuMobile Component', () => {
  it('renders the component with the mobile-menu2 class', () => {
    const { container } = render(
      <BrowserRouter>
        <LeftMenuMobile />
      </BrowserRouter>
    );

    expect(container.querySelector('.mobile-menu2')).toBeInTheDocument();
  });
});
