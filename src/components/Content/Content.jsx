import React, { useEffect, useState } from 'react';
import { InView } from 'react-intersection-observer';
import { useDispatch, useSelector } from 'react-redux';
import {
  incrementPage,
  setKeyword,
  useFetchColorsQuery,
  useFetchManufacturersQuery,
  useFetchMaterialsQuery,
  useFetchVrscansQuery
} from '../../store';
import { priceGenerator } from '../../utils/PriceGenerator';
import ItemCard from '../ItemCard/ItemCard';
import SearchBar from '../SearchBar/SearchBar';
import './Content.scss';

function Content() {
  const [allItemsLoaded, setAllItemsLoaded] = useState(false);

  const dispatch = useDispatch();

  const { data: colors } = useFetchColorsQuery();
  const { data: materials } = useFetchMaterialsQuery();
  const { data: manufacturers } = useFetchManufacturersQuery();

  const { filters } = useSelector((state) => {
    return {
      filters: state.filters
    };
  });

  const {
    data: vrscansNew,
    error: vrscansError,
    isFetching: isFetchingVrscans
  } = useFetchVrscansQuery(filters);

  const [vrscansAll, setVrscansAll] = useState([]);

  useEffect(() => {
    //console.log('filters!', filters);
    setAllItemsLoaded(false);
  }, [filters]);

  useEffect(() => {
    if (!isFetchingVrscans) {
      if (filters.page > 1) {
        if (vrscansNew?.length) {
          // add next page items
          setVrscansAll((prevData) => [...prevData, ...attachPriceToVrScans(vrscansNew)]);
        } else {
          // if no items are found, stop loading more pages
          setAllItemsLoaded(true);
        }
      } else {
        // set first page items
        setVrscansAll([...attachPriceToVrScans(vrscansNew)]);
      }
    }
  }, [isFetchingVrscans, vrscansNew, filters]);

  async function loadMoreItems() {
    if (vrscansNew) dispatch(incrementPage());
  }

  const manufacturerName = (manufacturerId, manufacturers) => {
    if (!manufacturers?.length) return false;
    const manufac = manufacturers.filter((manufacture) => manufacture?.id == manufacturerId);
    return manufac[0]?.name;
  };

  const materialName = (materialId, materials) => {
    if (!materials?.length) return false;
    const material = materials.filter((material) => material?.id == materialId);
    return material[0]?.name;
  };

  const handleSearch = (newSearchQuery) => {
    dispatch(setKeyword(newSearchQuery));
  };

  const attachPriceToVrScans = (vrScansData) => {
    const vrScansDataNew = [...vrScansData].map((vrs) => {
      return { ...vrs, price: priceGenerator() };
    });
    return vrScansDataNew;
  };

  const renderLoading = () => {
    return <div>Loading...</div>;
  };

  const renderError = () => {
    return <div>Error loading items.</div>;
  };

  const renderItems = () => {
    if (filters && !vrscansNew?.length && !vrscansAll?.length)
      return <>No VrScans found, matching your criteria.</>;
    return (
      <>
        {vrscansAll?.map((item) => (
          <div
            key={item?.id}
            className="col-6 col-sm-4 col-md-4 col-lg-4 col-xxl-3 item-card-outer"
          >
            <ItemCard
              item={item}
              title={item?.name}
              image={item?.thumb}
              manufacturerName={manufacturerName(item?.manufacturerId, manufacturers)}
              materialName={materialName(item?.materialTypeId, materials)}
            />
          </div>
        ))}

        <InView
          as="div"
          onChange={(inView) => {
            if (!allItemsLoaded && inView) {
              loadMoreItems();
            }
          }}
        />
      </>
    );
  };
  return (
    <>
      <div className="under-top-munu">
        <div className="under-top-munu--left">
          <p>
            <i>
              You can choose from <b>2246</b> VrScans
            </i>
          </p>
        </div>
        <SearchBar handleSearch={handleSearch} />
        <div className="dropup">
          <button
            type="button"
            className="btn dropdown-toggle"
            data-bs-toggle="dropdown"
            style={{ width: '100%' }}
          >
            Color
          </button>

          <ul className="dropdown-menu dropdown-menu-end ">
            <div className="filters-color">
              {colors?.map((item) => (
                <li key={item.id}>
                  <input
                    className="styled-checkbox"
                    id={`styled-checkbox-color${item.id}`}
                    type="checkbox"
                    name={item.id}
                  />
                  <label htmlFor={`styled-checkbox-color${item.id}`}>
                    {' '}
                    <span>{item.name}</span>
                  </label>
                </li>
              ))}
            </div>
          </ul>
        </div>
      </div>
      <div className="content" id="content">
        <div className="container">
          <div className="row ">
            {(isFetchingVrscans && filters?.page <= 1 && renderLoading()) ||
              (vrscansError && renderError()) ||
              renderItems()}
          </div>
        </div>
      </div>
    </>
  );
}

export default Content;
