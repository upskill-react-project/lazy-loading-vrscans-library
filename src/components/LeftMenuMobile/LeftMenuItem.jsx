import React from 'react';
import './LeftMenuItem.scss';

const LeftMenuItem = ({ tag, expanded, items, checkedItems, handleChange }) => {
  return (
    <div className="mobilemenu-filters">
      <div className="mobilemenu-filters-inn">
        <div className="accordion-body">
          <ul className="menu-filters">
            {items.map((item) => (
              <li key={item.id}>
                <input
                  className="styled-checkbox"
                  id={`styled-checkbox-${tag}${item.id}`}
                  type="checkbox"
                  name={item.id}
                  checked={checkedItems.includes(item.id)}
                  onChange={handleChange}
                />
                <label htmlFor={`styled-checkbox-${tag}${item.id}`}>
                  {' '}
                  <span>{item.name}</span>
                </label>
              </li>
            ))}
          </ul>
        </div>
      </div>
    </div>
  );
};

export default LeftMenuItem;
