import React, { useEffect, useState } from 'react';
import { useFetchColorsQuery, useFetchMaterialsQuery, useFetchTagsQuery } from '../../store';
import LeftMenuItem from './LeftMenuItem';
import './LeftMenuMobile.scss';

function LeftMenuMobile({ handleFilters }) {
  const {
    data: materials,
    error: materialsError,
    isFetching: isFetchingMaterials
  } = useFetchMaterialsQuery();
  const { data: colors, error: colorsError, isFetching: isFetchingColors } = useFetchColorsQuery();
  const { data: tags, error: tagsError, isFetching: isFetchingTags } = useFetchTagsQuery();

  const [menuFilters, setMenuFilters] = useState({ colors: [], tags: [], materials: [] });
  handleFilters(menuFilters);
  useEffect(() => {
    handleFilters(menuFilters);
  }, [menuFilters]);

  const updateCheckedItems = (checkedItems, event) => {
    const itemId = +event.target.name;
    if (event.target.checked === true) {
      // check
      checkedItems = [...checkedItems, itemId];
    } else {
      // uncheck
      checkedItems = checkedItems.filter(function (item) {
        return item !== itemId;
      });
    }
    return checkedItems;
  };

  const handleMaterials = (event) => {
    const newMaterials = updateCheckedItems(menuFilters.materials, event);
    const newMenuFilters = { ...menuFilters, materials: newMaterials };
    setMenuFilters(newMenuFilters);
  };

  const handleTags = (event) => {
    //console.log('tag clicked');
    const newTags = updateCheckedItems(menuFilters.tags, event);
    const newMenuFilters = { ...menuFilters, tags: newTags };
    setMenuFilters(newMenuFilters);
  };

  const handleColors = (event) => {
    const newColors = updateCheckedItems(menuFilters.colors, event);
    const newMenuFilters = { ...menuFilters, colors: newColors };
    setMenuFilters(newMenuFilters);
  };

  return (
    <div className="outer-mobile-menu">
      <div className="d-flex mobile-menu">
        <div className="dropdown">
          <button
            type="button"
            className="btn dropdown-toggle"
            data-bs-toggle="dropdown"
            style={{ width: '100%' }}
          >
            Materials
          </button>
          <ul
            className="dropdown-menu show"
            style={{
              position: 'absolute',
              zIndex: '800',
              inset: 'auto 0px 0px auto',
              margin: '0px',
              transform: 'translate3d(0px, 55px, 0px)'
            }}
          >
            <LeftMenuItem
              title="Materials"
              tag="Material"
              expanded={true}
              items={materials}
              isLoading={isFetchingMaterials}
              loadingError={materialsError}
              checkedItems={menuFilters.materials}
              handleChange={(event) => handleMaterials(event)}
            />
          </ul>
        </div>

        <div className="dropdown">
          <button
            type="button"
            className="btn dropdown-toggle"
            data-bs-toggle="dropdown"
            style={{ width: '100%' }}
          >
            Tags
          </button>
          <ul className="dropdown-menu dropdown-menu-end">
            <LeftMenuItem
              title="Tags"
              tag="Tag"
              expanded={false}
              items={tags}
              isLoading={isFetchingTags}
              loadingError={tagsError}
              checkedItems={menuFilters.tags}
              handleChange={(event) => handleTags(event)}
            />
          </ul>
        </div>
      </div>
    </div>
  );
}

export default LeftMenuMobile;
