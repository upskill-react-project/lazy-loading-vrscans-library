import React, { useContext } from 'react';
import { useSelector } from 'react-redux';
import { useMediaQuery } from 'react-responsive';
import { Link, useNavigate } from 'react-router-dom';
import { ShopContext } from '../../context/ShopContextProvider';
import useAuthWithLocalStorage from '../../hooks/useAuthWithLocalStorage';
import './TopMenu.scss';

function TopMenu() {
  const { logOut, logIn, isAuthorised, profile } = useAuthWithLocalStorage();

  const { cartItems } = useContext(ShopContext);
  const navigate = useNavigate();
  const isTablet = useMediaQuery({ query: '(max-width: 991px)' });
  const isMobile = useMediaQuery({ query: '(max-width: 500px)' });

  const RenderAuth = () => {
    return (
      <>
        {isAuthorised ? (
          <div className="authentication">
            <span className="sign-out-link" onClick={logOut} role="button">
              Sign out
            </span>

            <Link to="profile" className="profile-circle navbar-brand">
              <img
                src={profile.picture}
                alt={profile.name}
                className="navbar-image rounded-circle"
                width="30"
                height="30"
              />
            </Link>
            {!isTablet ? <span className="profile-link">{profile.email}</span> : ''}
          </div>
        ) : (
          <div className="authentication">
            {!isMobile ? (
              <span className="signin-link" onClick={logIn} role="button">
                Sign in /<span className="register-link"> Register </span>
              </span>
            ) : (
              ''
            )}
            <svg
              xmlns="http://www.w3.org/2000/svg"
              height="33"
              width="33"
              fill="white"
              viewBox="0 0 512 512"
            >
              <path d="M406.5 399.6C387.4 352.9 341.5 320 288 320H224c-53.5 0-99.4 32.9-118.5 79.6C69.9 362.2 48 311.7 48 256C48 141.1 141.1 48 256 48s208 93.1 208 208c0 55.7-21.9 106.2-57.5 143.6zm-40.1 32.7C334.4 452.4 296.6 464 256 464s-78.4-11.6-110.5-31.7c7.3-36.7 39.7-64.3 78.5-64.3h64c38.8 0 71.2 27.6 78.5 64.3zM256 512A256 256 0 1 0 256 0a256 256 0 1 0 0 512zm0-272a40 40 0 1 1 0-80 40 40 0 1 1 0 80zm-88-40a88 88 0 1 0 176 0 88 88 0 1 0 -176 0z" />
            </svg>
            &nbsp;&nbsp;<span className="separator">|</span>&nbsp;&nbsp;
          </div>
        )}
      </>
    );
  };

  const handleLoginOrRedirect = (path) => {
    if (isAuthorised) {
      return () => {
        navigate(path);
      };
    } else {
      return logIn;
    }
  };
  const favProducts = useSelector((state) => state.fav);

  const RenderFavorites = () => {
    return (
      <div className="favorites" onClick={handleLoginOrRedirect('/favorites')}>
        <Link className="favorites-link">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="white"
            height="33"
            width="37"
            viewBox="0 0 512 512"
          >
            <path d="M225.8 468.2l-2.5-2.3L48.1 303.2C17.4 274.7 0 234.7 0 192.8v-3.3c0-70.4 50-130.8 119.2-144C158.6 37.9 198.9 47 231 69.6c9 6.4 17.4 13.8 25 22.3c4.2-4.8 8.7-9.2 13.5-13.3c3.7-3.2 7.5-6.2 11.5-9c0 0 0 0 0 0C313.1 47 353.4 37.9 392.8 45.4C462 58.6 512 119.1 512 189.5v3.3c0 41.9-17.4 81.9-48.1 110.4L288.7 465.9l-2.5 2.3c-8.2 7.6-19 11.9-30.2 11.9s-22-4.2-30.2-11.9zM239.1 145c-.4-.3-.7-.7-1-1.1l-17.8-20c0 0-.1-.1-.1-.1c0 0 0 0 0 0c-23.1-25.9-58-37.7-92-31.2C81.6 101.5 48 142.1 48 189.5v3.3c0 28.5 11.9 55.8 32.8 75.2L256 430.7 431.2 268c20.9-19.4 32.8-46.7 32.8-75.2v-3.3c0-47.3-33.6-88-80.1-96.9c-34-6.5-69 5.4-92 31.2c0 0 0 0-.1 .1s0 0-.1 .1l-17.8 20c-.3 .4-.7 .7-1 1.1c-4.5 4.5-10.6 7-16.9 7s-12.4-2.5-16.9-7z" />
          </svg>
          <span className="fav-count">{favProducts.length}</span>
        </Link>
        &nbsp;
      </div>
    );
  };

  const RenderCart = () => {
    return (
      <div className="cart" onClick={handleLoginOrRedirect('/shopping-cart')}>
        <Link className="cart-link">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="37"
            height="33"
            fill="white"
            className="cart-icon"
            viewBox="0 0 16 16"
          >
            {' '}
            <path d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .49.598l-1 5a.5.5 0 0 1-.465.401l-9.397.472L4.415 11H13a.5.5 0 0 1 0 1H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM3.102 4l.84 4.479 9.144-.459L13.89 4H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 1 0 2 1 1 0 0 1 0-2zm7 0a1 1 0 1 1 0 2 1 1 0 0 1 0-2z" />{' '}
          </svg>
          <span className="cart-count">{cartItems.length}</span>
        </Link>
        &nbsp;
      </div>
    );
  };

  return (
    <>
      <div className="topmenu d-flex rounded-bottom justify-content-between py-2">
        <Link to="/">
          <div className="logo">
            <img src={'https://misha.krusharski.com/files/VR-Scans.png'} alt="VR Scans" />
          </div>
        </Link>
        <div className="auth-fav-cart d-flex">
          <RenderAuth />
          <RenderFavorites />
          <RenderCart />
        </div>
      </div>
    </>
  );
}

export default TopMenu;
