import { configureStore } from '@reduxjs/toolkit';
import { setupListeners } from '@reduxjs/toolkit/query';
import { colorsApi } from './apis/colorsApi';
import { manufacturersApi } from './apis/manufacturersApi';
import { materialsApi } from './apis/materialsApi';
import { tagsApi } from './apis/tagsApi';
import { vrscansApi } from './apis/vrscansApi';
import favSlice, { addFav, removeFav } from './slices/favSlice';
import filtersSlice, {
  decrementPage,
  incrementPage,
  setColors,
  setKeyword,
  setMaterials,
  setPage,
  setTags
} from './slices/filtersSlice';

const store = configureStore({
  reducer: {
    fav: favSlice,
    filters: filtersSlice,
    vrscans: vrscansApi.reducer,
    colors: colorsApi.reducer,
    materials: materialsApi.reducer,
    tags: tagsApi.reducer,
    manufacturers: manufacturersApi.reducer
  },
  middleware: (getDefaultMiddleware) => {
    return getDefaultMiddleware()
      .concat(vrscansApi.middleware)
      .concat(colorsApi.middleware)
      .concat(materialsApi.middleware)
      .concat(tagsApi.middleware)
      .concat(manufacturersApi.middleware);
  }
});

setupListeners(store.dispatch);

export default store;
export { useFetchColorsQuery } from './apis/colorsApi';
export { useFetchManufacturersQuery } from './apis/manufacturersApi';
export { useFetchMaterialsQuery } from './apis/materialsApi';
export { useFetchTagsQuery } from './apis/tagsApi';
export { useFetchVrscansQuery } from './apis/vrscansApi';
export {
  addFav,
  decrementPage,
  incrementPage,
  removeFav,
  setColors,
  setKeyword,
  setMaterials,
  setPage,
  setTags
};
