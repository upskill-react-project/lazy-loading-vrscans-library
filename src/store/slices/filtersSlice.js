import { createSlice } from '@reduxjs/toolkit';
// import { addCar } from './carsSlice';

const filtersSlice = createSlice({
  name: 'filters',
  initialState: {
    keyword: '',
    colors: [],
    tags: [],
    materials: [],
    page: 1
  },
  reducers: {
    setKeyword(state, action) {
      state.keyword = action.payload;
      state.page = 1;
    },
    setColors(state, action) {
      // return action.payload;
      return {
        ...state,
        colors: action.payload,
        page: 1
      };
    },
    setTags(state, action) {
      return {
        ...state,
        tags: action.payload,
        page: 1
      };
    },
    setMaterials(state, action) {
      // return action.payload;
      return {
        ...state,
        materials: action.payload,
        page: 1
      };
    },
    setPage(state, action) {
      state.page = action.payload;
    },
    incrementPage(state) {
      state.page++;
    },
    decrementPage(state) {
      state.page--;
    }
  }
});

export const {
  setKeyword,
  setColors,
  setTags,
  setMaterials,
  setPage,
  incrementPage,
  decrementPage
} = filtersSlice.actions;
export default filtersSlice.reducer;
