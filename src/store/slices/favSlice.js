import { createSlice } from '@reduxjs/toolkit';

const cartlocaldata = localStorage.getItem('cartitems');
const arraycart = JSON.parse(cartlocaldata);

const initialState = arraycart ? arraycart : [];

const favSlice = createSlice({
  name: 'fav',
  initialState,
  reducers: {
    addFav(state, action) {
      const item = action.payload;
      const existingItem = state.find((product) => product.id === item.id);
      if (!existingItem) {
        state.push(item);
      }
      localStorage.setItem('cartitems', JSON.stringify(state));
    },
    removeFav(state, action) {
      const newState = state.filter((product) => product.id !== action.payload);
      localStorage.setItem('cartitems', JSON.stringify(newState));
      return newState;
    }
  }
});

export const { addFav, removeFav } = favSlice.actions;
export default favSlice.reducer;
