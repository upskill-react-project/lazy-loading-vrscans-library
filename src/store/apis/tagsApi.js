import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { endPoints } from '../../api/api.config';

const tagsApi = createApi({
  reducerPath: 'tags',
  baseQuery: fetchBaseQuery({
    baseUrl: process.env.REACT_APP_API_BASE_URL
  }),
  endpoints(builder) {
    return {
      fetchTags: builder.query({
        query: () => {
          return {
            url: endPoints.tags,
            method: 'GET'
          };
        }
      })
    };
  }
});

export const { useFetchTagsQuery } = tagsApi;
export { tagsApi };
