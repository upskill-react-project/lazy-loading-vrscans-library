import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { endPoints } from '../../api/api.config';

const materialsApi = createApi({
  reducerPath: 'materials',
  baseQuery: fetchBaseQuery({
    baseUrl: process.env.REACT_APP_API_BASE_URL
  }),
  endpoints(builder) {
    return {
      fetchMaterials: builder.query({
        query: () => {
          return {
            url: endPoints.materials,
            method: 'GET'
          };
        }
      })
    };
  }
});

export const { useFetchMaterialsQuery } = materialsApi;
export { materialsApi };
