import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { endPoints } from '../../api/api.config';

const colorsApi = createApi({
  reducerPath: 'colors',
  baseQuery: fetchBaseQuery({
    baseUrl: process.env.REACT_APP_API_BASE_URL
  }),
  endpoints(builder) {
    return {
      fetchColors: builder.query({
        query: () => {
          return {
            url: endPoints.colors,
            method: 'GET'
          };
        }
      })
    };
  }
});

export const { useFetchColorsQuery } = colorsApi;
export { colorsApi };
