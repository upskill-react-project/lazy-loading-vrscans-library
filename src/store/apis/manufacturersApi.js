import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { endPoints } from '../../api/api.config';

const manufacturersApi = createApi({
  reducerPath: 'manufacturers',
  baseQuery: fetchBaseQuery({
    baseUrl: process.env.REACT_APP_API_BASE_URL
  }),
  endpoints(builder) {
    return {
      fetchManufacturers: builder.query({
        query: () => {
          return {
            url: endPoints.manufacturers,
            method: 'GET'
          };
        }
      })
    };
  }
});

export const { useFetchManufacturersQuery } = manufacturersApi;
export { manufacturersApi };
