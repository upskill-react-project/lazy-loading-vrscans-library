import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { endPoints } from '../../api/api.config';
import { paramsConstructFromFilters } from '../../utils/paramsConstructFromFilters';

const vrscansApi = createApi({
  reducerPath: 'vrscans',
  baseQuery: fetchBaseQuery({
    baseUrl: process.env.REACT_APP_API_BASE_URL
  }),
  endpoints(builder) {
    return {
      fetchVrscans: builder.query({
        query: (filters) => {
          const params = paramsConstructFromFilters(filters);
          //console.log('vrscansApi in function query params : ', params);
          return {
            url: endPoints.vrscans,
            params: params,
            method: 'GET'
          };
        }
      })
    };
  }
});

export const { useFetchVrscansQuery } = vrscansApi;
export { vrscansApi };
