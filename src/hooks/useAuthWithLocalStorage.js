import { useAuth0 } from '@auth0/auth0-react';

const useAuthWithLocalStorage = () => {
  const { user, isLoading, loginWithRedirect, logout, isAuthenticated } = useAuth0();
  let profile, isAuthorised;

  const logIn = () => {
    loginWithRedirect();
  };

  const logOut = () => {
    localStorage.removeItem('userData');
    localStorage.removeItem('cartItems');
    logout({ logoutParams: { returnTo: window.location.origin } });
  };

  if (isAuthenticated) {
    localStorage.setItem('userData', JSON.stringify(user));
  }

  let userData = localStorage.getItem('userData');
  if (userData) {
    profile = JSON.parse(userData);
  }

  isAuthorised = !!userData;

  return { logIn, logOut, isLoading, profile, isAuthorised };
};

export default useAuthWithLocalStorage;
