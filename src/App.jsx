import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import './App.scss';
import TopMenu from './components/TopMenu/TopMenu';
import ShopContextProvider from './context/ShopContextProvider';
import useAuthWithLocalStorage from './hooks/useAuthWithLocalStorage';
import AuthenticatedRoutes from './routes/AuthenticatedRoutes';
import ProtectedRoute from './routes/ProtectedRoute';
import UnauthenticatedRoutes from './routes/UnauthenticatedRoutes';

function App() {
  const { isLoading } = useAuthWithLocalStorage();

  if (isLoading) {
    return <div>Loading ...</div>;
  }

  return (
    <BrowserRouter>
      <ShopContextProvider>
        <div className="outer">
          <div className="container top-menu-outer ">
            <TopMenu />
          </div>
          <div className="container" style={{ position: 'relative' }}>
            <UnauthenticatedRoutes />
            <ProtectedRoute>
              <AuthenticatedRoutes />
            </ProtectedRoute>
          </div>
        </div>
      </ShopContextProvider>
    </BrowserRouter>
  );
}

export default App;
