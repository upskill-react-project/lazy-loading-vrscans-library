export const paramsConstructFromFilters = ({ keyword, colors, tags, materials, page }) => {
  const params = new URLSearchParams();
  params.append('_page', page > 0 ? page : 1);
  params.append('_limit', 20);
  // params.append('_page', pageRef.current);
  if (keyword) params.append('name_like', keyword);
  if (materials?.length) {
    materials.forEach((material) => {
      params.append('materialTypeId', material);
    });
  }
  if (tags?.length) {
    tags.forEach((tag) => {
      params.append('tags_like', tag);
    });
  }
  if (colors?.length) {
    colors.forEach((color) => {
      params.append('colors_like', color);
    });
  }
  return params;
};
