import { paramsConstructFromFilters } from './paramsConstructFromFilters';

describe('paramsConstructFromFilters', () => {
  it('should correctly construct params with all inputs provided', () => {
    const filters = {
      keyword: 'testKeyword',
      colors: ['red', 'blue'],
      tags: ['tag1', 'tag2'],
      materials: ['wood', 'metal'],
      page: 2
    };
    const params = paramsConstructFromFilters(filters);
    expect(params.get('_page')).toBe('2');
    expect(params.get('_limit')).toBe('20');
    expect(params.get('name_like')).toBe('testKeyword');
    expect(params.getAll('materialTypeId')).toEqual(['wood', 'metal']);
    expect(params.getAll('tags_like')).toEqual(['tag1', 'tag2']);
    expect(params.getAll('colors_like')).toEqual(['red', 'blue']);
  });

  it('should set default values when no inputs are provided', () => {
    const params = paramsConstructFromFilters({});
    expect(params.get('_page')).toBe('1');
    expect(params.get('_limit')).toBe('20');
    expect(params.get('name_like')).toBeNull();
  });

  it('should handle missing parameters', () => {
    const filters = {
      keyword: 'testKeyword',
      page: 3
    };
    const params = paramsConstructFromFilters(filters);
    expect(params.get('_page')).toBe('3');
    expect(params.get('_limit')).toBe('20');
    expect(params.get('name_like')).toBe('testKeyword');
    expect(params.get('materialTypeId')).toBeNull();
    expect(params.get('tags_like')).toBeNull();
    expect(params.get('colors_like')).toBeNull();
  });

  it('should default to page 1 if page is less than 1', () => {
    const filters = {
      page: 0
    };
    const params = paramsConstructFromFilters(filters);
    expect(params.get('_page')).toBe('1');
  });

  it('should not include materials, tags, or colors if they are empty arrays', () => {
    const filters = {
      materials: [],
      tags: [],
      colors: [],
      page: 1
    };
    const params = paramsConstructFromFilters(filters);
    expect(params.get('materialTypeId')).toBeNull();
    expect(params.get('tags_like')).toBeNull();
    expect(params.get('colors_like')).toBeNull();
  });
});
