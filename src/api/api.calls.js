import { api } from './api.config';

export async function getAllApi(endpoint, filters = null) {
  return await api.get(`${endpoint}`, { params: filters });
}

export async function getApi(endpoint, id) {
  return await api.get(`${endpoint}/${id}`);
}

export async function deleteApi(endpoint, id) {
  return await api.delete(`${endpoint}/${id}`);
}

export async function postApi(endpoint, arg) {
  return await api.post(`${endpoint}`, arg);
}

export async function putApi(endpoint, id, arg) {
  return (await api.put)(`${endpoint}/${id}`, arg);
}
