import axios from 'axios';

const api = axios.create({
  baseURL: 'https://json-server-test-7c6787734c77.herokuapp.com/'
});

const endPoints = {
  materials: 'materials',
  manufacturers: 'manufacturers',
  industries: 'industries',
  colors: 'colors',
  tags: 'tags',
  vrscans: 'vrscans',
  favorites: 'favorites',
  carditems: 'carditems',
  users: 'users'
};

export { api, endPoints };
