// AuthenticatedRoutes.jsx
import React from 'react';
import { Route, Routes } from 'react-router-dom';
import Profile from '../containers/ProfilePage/Profile';
import ShoppingCart from '../containers/ShoppingCart/ShoppingCart';
import FavoritesPage from '../containers/FavoritesPage/FavoritesPage';
import ItemDetails from '../components/ItemDetails/ItemDetails';

const AuthenticatedRoutes = () => (
  <Routes>
    <Route path="/profile" element={<Profile />} />
    <Route exact path="/favorites" element={<FavoritesPage />} />
    <Route path="/item/:id" element={<ItemDetails />} />
    <Route path="/shopping-cart" element={<ShoppingCart />} />
  </Routes>
);
export default AuthenticatedRoutes;
