import React from 'react';
import { Navigate } from 'react-router-dom';
import useAuthWithLocalStorage from '../hooks/useAuthWithLocalStorage';

// eslint-disable-next-line react/prop-types
const ProtectedRoute = ({ children }) => {
  const { isAuthorised } = useAuthWithLocalStorage();

  return isAuthorised ? children : <Navigate to="/" />;
};

export default ProtectedRoute;
