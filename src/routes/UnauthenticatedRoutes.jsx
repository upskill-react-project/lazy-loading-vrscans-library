import React from 'react';
import { Route, Routes } from 'react-router-dom';
import Home from '../containers/HomePage/Home';

const UnauthenticatedRoutes = () => (
  <Routes>
    <Route exact path="/" element={<Home />} />
  </Routes>
);
export default UnauthenticatedRoutes;
