import React, { useState } from 'react';
import { useMediaQuery } from 'react-responsive';
import Content from '../../components/Content/Content';
import LeftMenu from '../../components/LeftMenu/LeftMenu';
import LeftMenuMobile from '../../components/LeftMenuMobile/LeftMenuMobile';

const Home = () => {
  const isTablet = useMediaQuery({ query: '(max-width: 992px)' });

  const [filters, setFilters] = useState('');

  function handleFilters(newFilters) {
    setFilters(newFilters);
    //console.log('filtersInHomeComponent', filters);
  }

  return (
    <>
      {!isTablet ? (
        <LeftMenu handleFilters={handleFilters} />
      ) : (
        <LeftMenuMobile handleFilters={handleFilters} />
      )}
      <div className="homepage">
        <Content filters={filters} />
      </div>
    </>
  );
};

export default Home;
