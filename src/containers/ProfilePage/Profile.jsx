import React, { useState } from 'react';
import { Card, Image, Form, Button } from 'react-bootstrap';
import { useMediaQuery } from 'react-responsive';
import LeftMenuNoFilters from '../../components/LeftMenuNoFilters/LeftMenuNoFilters';
import LeftMenuNoFiltersMobile from '../../components/LeftMenuNoFilters/LeftMenuNoFiltersMobile';
import useAuthWithLocalStorage from '../../hooks/useAuthWithLocalStorage';
import { editProfile } from './EditService';
import './Profile.scss';
import { formatDate } from '../../utils/formatDate';

const Profile = () => {
  const { profile } = useAuthWithLocalStorage();
  const [isEditing, setIsEditing] = useState(false);
  const [successMessage, setSuccessMessage] = useState('');
  const [errorMessage, setErrorMessage] = useState('');

  const [editedProfile, setEditedProfile] = useState({
    nickname: profile.nickname,
    email: profile.email,
    picture: profile.picture
  });
  const isTablet = useMediaQuery({ query: '(max-width: 992px)' });

  const handleInputChange = (e) => {
    setEditedProfile({ ...editedProfile, [e.target.name]: e.target.value });
  };

  async function handleSubmit(event) {
    event.preventDefault();

    try {
      const userId = profile.sub;

      const userData = {
        email: editedProfile.email,
        nickname: editedProfile.nickname,
        password: editedProfile.password
      };

      const editedData = await editProfile(userId, userData);

      const transformObject = (obj) => {
        if ('user_id' in obj) {
          obj.sub = obj.user_id;
          delete obj.user_id;
        }
        return obj;
      };

      localStorage.setItem('userData', JSON.stringify(transformObject(editedData)));
      setSuccessMessage('Field successfully changed!');
    } catch (error) {
      console.error(error);
      setErrorMessage('Could not update the filed!');
    }
  }

  return (
    <>
      {!isTablet ? (
        <LeftMenuNoFilters classparam={'leftmenu-proba'} />
      ) : (
        <LeftMenuNoFiltersMobile />
      )}

      <div className="content c2 content-nmt">
        <div className="container">
          <Card className="profile-container">
            <Card.Header className="profile-background">
              <div className="d-flex justify-content-center">
                <Image
                  src={profile.picture}
                  alt={profile.name}
                  roundedCircle
                  fluid
                  className="my-3 w-25"
                />
              </div>
            </Card.Header>

            <Card.Header className="p-0">
              {isEditing ? (
                <>
                  <div className="profile-name d-flex justify-content-center align-items-center">
                    <div className="profile-name-wrapper">
                      <span className="nickname">Edit your profile</span>
                    </div>
                  </div>
                  <Form onSubmit={handleSubmit}>
                    <Form.Group>
                      <Form.Label className="d-flex justify-content-center align-items-center mt-2 mb-1">
                        Name
                      </Form.Label>
                      <div className="d-flex justify-content-center align-items-center">
                        <Form.Control
                          className="profile-input"
                          type="text"
                          name="nickname"
                          value={editedProfile?.nickname}
                          onChange={handleInputChange}
                        />
                      </div>

                      <Form.Label className="d-flex justify-content-center align-items-center mt-2 mb-1">
                        Email
                      </Form.Label>
                      <div className="d-flex justify-content-center align-items-center">
                        <Form.Control
                          className="profile-input"
                          type="email"
                          name="email"
                          value={editedProfile?.email}
                          onChange={handleInputChange}
                        />
                      </div>
                    </Form.Group>
                    <div className="d-flex justify-content-center align-items-center my-3">
                      {successMessage ? (
                        <p className="text-success">{successMessage}</p>
                      ) : (
                        <p className="text-danger">{errorMessage}</p>
                      )}
                    </div>
                    <div className="d-flex justify-content-center align-items-center my-3">
                      <Button variant="primary" type="submit" className="btn profile-save">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="25"
                          height="25"
                          fill="currentColor"
                          className="bi bi-check"
                          viewBox="0 0 16 16"
                        >
                          <path d="M10.97 4.97a.75.75 0 0 1 1.07 1.05l-3.99 4.99a.75.75 0 0 1-1.08.02L4.324 8.384a.75.75 0 1 1 1.06-1.06l2.094 2.093 3.473-4.425z" />
                        </svg>
                      </Button>
                      <Button
                        variant="secondary"
                        onClick={() => setIsEditing(false)}
                        className="profile-cancel"
                      >
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="25"
                          height="25"
                          fill="currentColor"
                          className="bi bi-x"
                          viewBox="0 0 16 16"
                        >
                          <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708" />
                        </svg>
                      </Button>
                    </div>
                  </Form>
                </>
              ) : (
                <>
                  <div className="profile-name d-flex justify-content-center align-items-center">
                    <div className="profile-name-wrapper">
                      <span className="name-label">Name:</span>
                      <span className="nickname">{editedProfile?.nickname}</span>
                      <span className="pencil-pointer" onClick={() => setIsEditing(true)}>
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          viewBox="0 0 512 512"
                          width="20px"
                          className="profile-edit-pencil"
                        >
                          <path
                            fill="white"
                            d="M410.3 231l11.3-11.3-33.9-33.9-62.1-62.1L291.7 89.8l-11.3 11.3-22.6 22.6L58.6 322.9c-10.4 10.4-18 23.3-22.2 37.4L1 480.7c-2.5 8.4-.2 17.5 6.1 23.7s15.3 8.5 23.7 6.1l120.3-35.4c14.1-4.2 27-11.8 37.4-22.2L387.7 253.7 410.3 231zM160 399.4l-9.1 22.7c-4 3.1-8.5 5.4-13.3 6.9L59.4 452l23-78.1c1.4-4.9 3.8-9.4 6.9-13.3l22.7-9.1v32c0 8.8 7.2 16 16 16h32zM362.7 18.7L348.3 33.2 325.7 55.8 314.3 67.1l33.9 33.9 62.1 62.1 33.9 33.9 11.3-11.3 22.6-22.6 14.5-14.5c25-25 25-65.5 0-90.5L453.3 18.7c-25-25-65.5-25-90.5 0zm-47.4 168l-144 144c-6.2 6.2-16.4 6.2-22.6 0s-6.2-16.4 0-22.6l144-144c6.2-6.2 16.4-6.2 22.6 0s6.2 16.4 0 22.6z"
                          />
                        </svg>
                      </span>
                    </div>
                  </div>

                  <div className="d-flex justify-content-center align-items-center mt-3">
                    <div className="profile-name-wrapper">
                      <span className="email-label">Email:</span>
                      <span className="email">{profile?.email}</span>
                    </div>
                  </div>
                  <div className="d-flex justify-content-center align-items-center mb-5">
                    <div className="profile-name-wrapper">
                      <span className="email-label">Last updated at: </span>
                      <span className="date">
                        {formatDate(profile.updated_at)}&nbsp; &nbsp; &nbsp;&nbsp;
                      </span>
                    </div>
                  </div>
                </>
              )}
            </Card.Header>
          </Card>
        </div>
      </div>
    </>
  );
};

export default Profile;
