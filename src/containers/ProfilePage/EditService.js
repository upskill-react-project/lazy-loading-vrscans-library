async function editProfile(userId, userData) {
  const tokens = await getTokens().then((response) => response.json());
  return updateData(tokens.access_token, userId, userData);
}

async function updateData(token, userId, userData) {
  const myHeaders = new Headers();
  myHeaders.append('Content-Type', 'application/json');
  myHeaders.append('Accept', 'application/json');
  myHeaders.append('Authorization', `Bearer ${token}`);

  const raw = JSON.stringify({
    blocked: userData.blocked,
    email_verified: userData.email_verified,
    email: userData.email,
    phone_number: userData.phone_number,
    phone_verified: userData.phone_verified,
    user_metadata: userData.user_metadata,
    app_metadata: userData.app_metadata,
    given_name: userData.given_name,
    family_name: userData.family_name,
    name: userData.name,
    nickname: userData.nickname,
    picture: userData.picture,
    verify_email: userData.verify_email,
    verify_phone_number: userData.verify_phone_number,
    password: userData.password,
    connection: userData.connection,
    client_id: userData.client_id,
    username: userData.username
  });

  const requestOptions = {
    method: 'PATCH',
    headers: myHeaders,
    body: raw,
    redirect: 'follow'
  };

  try {
    const url = `https://dev-5b0pjjfsyxvg5y7o.us.auth0.com/api/v2/users/${userId}`;
    return await fetch(url, requestOptions).then((response) => response.json());
  } catch (error) {
    return console.log('error', error);
  }
}

async function getTokens() {
  // bypass cors security
  const url =
    'https://corsproxy.io/?' +
    encodeURIComponent('https://dev-5b0pjjfsyxvg5y7o.us.auth0.com/oauth/token');
  const myHeaders = new Headers();
  myHeaders.append('content-type', 'application/json');

  const data = {
    client_id: 'nDmVdfrr4wfhc4p2yOW8hWpZQRU5l7dD',
    client_secret: 'M3ePaon3xWDljwyoa5Z31mJ-rTz-U77ZmzS2lTOmQnHqmx3coIV1V2CqhYtit0zY',
    audience: 'https://dev-5b0pjjfsyxvg5y7o.us.auth0.com/api/v2/',
    grant_type: 'client_credentials'
  };

  return fetch(url, {
    method: 'POST',
    headers: myHeaders,
    body: JSON.stringify(data)
  });
}

export { editProfile };
