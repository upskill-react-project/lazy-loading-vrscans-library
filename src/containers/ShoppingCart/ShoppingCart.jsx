import React, { useContext } from 'react';
import { Button } from 'react-bootstrap';
import { useMediaQuery } from 'react-responsive';
import { useNavigate } from 'react-router-dom';
import StripeCheckout from 'react-stripe-checkout';
import LeftMenuNoFilters from '../../components/LeftMenuNoFilters/LeftMenuNoFilters';
import LeftMenuNoFiltersMobile from '../../components/LeftMenuNoFilters/LeftMenuNoFiltersMobile';
import { ShopContext } from '../../context/ShopContextProvider';
import useAuthWithLocalStorage from '../../hooks/useAuthWithLocalStorage';
import CartItem from './CartItem';
import './ShoppingCart.scss';

const ShoppingCart = () => {
  const { cartItems, getTotalCartAmount, clearCart } = useContext(ShopContext);
  const { profile } = useAuthWithLocalStorage();

  const navigate = useNavigate();

  const totalAmount = getTotalCartAmount().toFixed(2);
  const totalAmountInCents = totalAmount * 100;

  const handleToken = (token) => {
    clearCart();
    navigate('/');
  };
  const isTablet = useMediaQuery({ query: '(max-width: 992px)' });

  return (
    <>
      <div className="under-top-munu">
        <h4 className="pageinfo">Items in the cart</h4>
      </div>

      {!isTablet ? <LeftMenuNoFilters /> : <LeftMenuNoFiltersMobile />}

      <div className="content c2">
        <div className="container">
          <div className="shoping-cart">
            <div className="shoping-cart--left">
              {totalAmount > 0 ? (
                <div>
                  {cartItems.map((item) => (
                    <div key={item.id} className="mb-3">
                      <CartItem
                        id={item.id}
                        title={item.title}
                        image={item.image}
                        manifactureName={item.manifactureName}
                        materialName={item.materialName}
                        price={item.price}
                      />
                    </div>
                  ))}
                </div>
              ) : (
                <>
                  <h4 style={{ paddingLeft: '20px' }}>Your chart is empty!</h4>
                </>
              )}
            </div>

            <div className="shoping-cart--right">
              {totalAmount > 0 ? (
                <>
                  <div className="d-flex flex-column align-items-center justify-content-center mt-5">
                    <h5>Subtotal: </h5>

                    <h2>$ {totalAmount}</h2>
                  </div>
                  <div className="d-flex justify-content-center mt-3 mb-2">
                    <StripeCheckout
                      token={handleToken}
                      stripeKey="pk_test_51OUSTKBREGzXUhFkAJspetyB7j0cqcNc7GumHk4z5GLx5ZuLVnU7ffVReowXommUDe2Eo2uadx8TKSo92Jm4gbH100gy7GY0NG"
                      name="VRScans"
                      description="Payment for VRScans"
                      amount={totalAmountInCents}
                      currency="USD"
                      email={profile.email}
                    >
                      <Button
                        variant="warning"
                        className="custom-warning-button cart-item"
                        size="md"
                      >
                        BUY NOW
                      </Button>
                    </StripeCheckout>
                  </div>
                  <div className="d-flex justify-content-center mb-3">
                    <Button
                      variant="warning"
                      className="custom-warning-button cart-item"
                      size="sm"
                      onClick={() => navigate('/')}
                    >
                      Continue Shopping
                    </Button>
                  </div>
                </>
              ) : (
                <></>
              )}
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default ShoppingCart;
