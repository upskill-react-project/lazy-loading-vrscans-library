import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom';
import CartItem from './CartItem';
import { ShopContext } from '../../context/ShopContextProvider';

const mockRemoveFromCart = jest.fn();
const mockData = {
  id: '1',
  title: 'Test Item',
  image: 'test-image.jpg',
  manifactureName: 'Test Manufacturer',
  materialName: 'Test Material',
  price: 100
};

describe('CartItem Component', () => {
  it('renders correctly with given props', () => {
    render(
      <ShopContext.Provider value={{ removeFromCart: mockRemoveFromCart }}>
        <CartItem {...mockData} />
      </ShopContext.Provider>
    );

    expect(screen.getByText('Test Item')).toBeInTheDocument();
    expect(screen.getByText('Test Manufacturer')).toBeInTheDocument();
    expect(screen.getByText('Test Material')).toBeInTheDocument();
    expect(screen.getByText('100$')).toBeInTheDocument();
    expect(screen.getByRole('img')).toHaveAttribute('src', 'test-image.jpg');
  });

  it('calls removeFromCart with correct id when remove button is clicked', () => {
    render(
      <ShopContext.Provider value={{ removeFromCart: mockRemoveFromCart }}>
        <CartItem {...mockData} />
      </ShopContext.Provider>
    );

    const removeButton = screen.getByText('Remove item');
    fireEvent.click(removeButton);
    expect(mockRemoveFromCart).toHaveBeenCalledWith('1');
  });
});
