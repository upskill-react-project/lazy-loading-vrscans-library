const prices = [10.99, 19.99, 5.49, 29.95, 8.99, 15.75, 12.49, 24.99, 7.25, 14.99];
export const priceGenerator = () => {
  if (!Array.isArray(prices) || prices.length === 0) {
    return null;
  }
  const randomIndex = Math.floor(Math.random() * prices.length);
  return prices[randomIndex];
};
