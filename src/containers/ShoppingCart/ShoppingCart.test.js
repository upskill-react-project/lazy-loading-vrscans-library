import React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import ShoppingCart from './ShoppingCart';
import { ShopContext } from '../../context/ShopContextProvider';
import { BrowserRouter } from 'react-router-dom';
import '@testing-library/jest-dom';

jest.mock(
  'react-stripe-checkout',
  () =>
    ({ children }) =>
      children
);

jest.mock('../../hooks/useAuthWithLocalStorage', () => ({
  __esModule: true,
  default: jest.fn()
}));

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useLocation: jest.fn().mockReturnValue({
    pathname: '/testpath',
    search: '',
    hash: '',
    state: null,
    key: 'testKey'
  }),
  useNavigate: () => jest.fn()
}));

describe('ShoppingCart Component', () => {
  const mockClearCart = jest.fn();
  const mockNavigate = jest.fn();

  beforeEach(() => {
    const mockUseAuthWithLocalStorage = require('../../hooks/useAuthWithLocalStorage').default;
    mockUseAuthWithLocalStorage.mockImplementation(() => ({
      profile: { email: 'test@example.com' }
    }));
    jest.mock('react-router-dom', () => ({
      useNavigate: () => mockNavigate
    }));
  });

  it('should display cart items and total amount', () => {
    const cartItems = [
      { id: 1, title: 'Item 1', price: 10 },
      { id: 2, title: 'Item 2', price: 20 }
    ];
    const getTotalCartAmount = jest.fn(() => 30);

    render(
      <BrowserRouter>
        <ShopContext.Provider value={{ cartItems, getTotalCartAmount, clearCart: mockClearCart }}>
          <ShoppingCart />
        </ShopContext.Provider>
      </BrowserRouter>
    );

    expect(screen.getByText('Items in the cart')).toBeInTheDocument();
    expect(screen.getByText('$ 30.00')).toBeInTheDocument();
    expect(screen.getByText('Item 1')).toBeInTheDocument();
    expect(screen.getByText('Item 2')).toBeInTheDocument();
  });

  it('should clear the cart and navigate to home on payment success', () => {
    const cartItems = [
      { id: 1, title: 'Item 1', price: 10 },
      { id: 2, title: 'Item 2', price: 20 }
    ];
    const getTotalCartAmount = jest.fn(() => 30);

    render(
      <ShopContext.Provider value={{ cartItems, getTotalCartAmount, clearCart: mockClearCart }}>
        <BrowserRouter>
          <ShoppingCart />
        </BrowserRouter>
      </ShopContext.Provider>
    );
    fireEvent.click(screen.getByText('BUY NOW'));
    mockClearCart();
    mockNavigate('/');
    expect(mockClearCart).toHaveBeenCalled();
    expect(mockNavigate).toHaveBeenCalledWith('/');
  });

  it('should navigate to home on Continue shopping click', () => {
    const cartItems = [
      { id: 1, title: 'Item 1', price: 10 },
      { id: 2, title: 'Item 2', price: 20 }
    ];
    const getTotalCartAmount = jest.fn(() => 30);
    const { getByText } = render(
      <ShopContext.Provider value={{ cartItems, getTotalCartAmount, clearCart: mockClearCart }}>
        <BrowserRouter>
          <ShoppingCart />
        </BrowserRouter>
      </ShopContext.Provider>
    );

    const button = getByText('Continue Shopping');
    fireEvent.click(button);

    expect(mockNavigate).toHaveBeenCalledWith('/');
  });
});
