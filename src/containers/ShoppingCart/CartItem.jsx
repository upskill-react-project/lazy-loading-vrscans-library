import React, { useContext } from 'react';
import { Card } from 'react-bootstrap';
import Button from 'react-bootstrap/Button';
import { ShopContext } from '../../context/ShopContextProvider';
import './CartItem.scss';

function CartItem({ id, title, image, manifactureName, materialName, price }) {
  const { removeFromCart } = useContext(ShopContext);
  return (
    <Card>
      <Card.Header as="h5" className="d-flex justify-content-center">
        {title}
      </Card.Header>

      <Card.Body>
        <div className="d-flex justify-content-center">
          <img src={image} />
        </div>
        <hr />
        <div className="d-flex justify-content-center">
          <Card.Title>{manifactureName}</Card.Title>
        </div>
        <div className="d-flex justify-content-center">
          <Card.Text>{materialName}</Card.Text>
        </div>
        <div className="d-flex justify-content-center">
          <Card.Title>{price}$</Card.Title>
        </div>
        <div className="d-flex justify-content-center">
          <Button
            variant="light"
            className="custom-light-button"
            onClick={() => removeFromCart(id)}
          >
            Remove item
          </Button>
        </div>
      </Card.Body>
    </Card>
  );
}

export default CartItem;
