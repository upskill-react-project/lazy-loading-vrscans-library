import React, { useContext } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useMediaQuery } from 'react-responsive';
import LeftMenuNoFilters from '../../components/LeftMenuNoFilters/LeftMenuNoFilters';
import LeftMenuNoFiltersMobile from '../../components/LeftMenuNoFilters/LeftMenuNoFiltersMobile';
import { ShopContext } from '../../context/ShopContextProvider';
import { useFetchManufacturersQuery, useFetchMaterialsQuery } from '../../store';
import { removeFav } from '../../store/';
import './FavoritesPage.scss';

const FavoritesPage = () => {
  const favProducts = useSelector((state) => state.fav);
  const dispatch = useDispatch();
  const removeFromFav = (id) => {
    dispatch(removeFav(id));
  };

  const { addToCart, removeFromCart, isItemInCart } = useContext(ShopContext);

  const { data: materials } = useFetchMaterialsQuery();
  const { data: manufacturers } = useFetchManufacturersQuery();

  function manufactureName(manufacturerId, manufacturers) {
    if (!manufacturers?.length) return false;
    const manifac = manufacturers.filter((manufacture) => manufacture?.id == manufacturerId);
    return manifac[0]?.name;
  }

  function materialName(materialId, materials) {
    if (!materials?.length) return false;
    const material = materials.filter((material) => material?.id == materialId);
    return material[0]?.name;
  }

  const handleClick = (item) => {
    const itemToBuy = {
      id: item.id,
      title: item.name,
      image: item.thumb,
      price: item.price,
      manufactureName: manufactureName(item?.manufacturerId, manufacturers),
      materialName: materialName(item?.materialTypeId, materials)
    };
    if (isItemInCart(item.id)) {
      removeFromCart(item.id);
    } else {
      addToCart(itemToBuy);
    }
  };

  const isTablet = useMediaQuery({ query: '(max-width: 992px)' });

  return (
    <>
      <div className="under-top-munu">
        <h4 className="pageinfo">Your Favorite Items</h4>
      </div>
      {!isTablet ? <LeftMenuNoFilters /> : <LeftMenuNoFiltersMobile />}

      <div className="content c2">
        <div className="container">
          <div className="row d-flex justify-content-left">
            {favProducts.length > 0 ? (
              favProducts.map((item) => (
                <div key={item.id} className="col-12 col-md-6  item__card-outer">
                  <div className="item__card">
                    <div className="item__card-image">
                      <img src={item.thumb} />
                    </div>

                    <div className="item__card-info">
                      <h1>{item.name}</h1>
                      <i>{manufactureName(item?.manufacturerId, manufacturers)}</i>
                      <span>{materialName(item?.materialTypeId, materials)}</span>
                      <div className="item__card-info--price">{item.price}$</div>
                    </div>
                    <div className="item__card-buttons">
                      <button className="button-fav" onClick={() => removeFromFav(item.id)}>
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          fill="#cbc168"
                          height="25"
                          width="29"
                          viewBox="0 0 512 512"
                        >
                          <path d="M47.6 300.4L228.3 469.1c7.5 7 17.4 10.9 27.7 10.9s20.2-3.9 27.7-10.9L464.4 300.4c30.4-28.3 47.6-68 47.6-109.5v-5.8c0-69.9-50.5-129.5-119.4-141C347 36.5 300.6 51.4 268 84L256 96 244 84c-32.6-32.6-79-47.5-124.6-39.9C50.5 55.6 0 115.2 0 185.1v5.8c0 41.5 17.2 81.2 47.6 109.5z" />
                        </svg>
                      </button>

                      <button
                        className={`shop-now-button ${
                          isItemInCart(item.id) ? 'cart-item-remove' : 'cart-item-added'
                        }`}
                        onClick={() => handleClick(item)}
                      >
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="18"
                          height="18"
                          className="cart-icon"
                          viewBox="0 0 16 16"
                        >
                          {' '}
                          <path d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .49.598l-1 5a.5.5 0 0 1-.465.401l-9.397.472L4.415 11H13a.5.5 0 0 1 0 1H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM3.102 4l.84 4.479 9.144-.459L13.89 4H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 1 0 2 1 1 0 0 1 0-2zm7 0a1 1 0 1 1 0 2 1 1 0 0 1 0-2z" />{' '}
                        </svg>
                      </button>
                    </div>
                  </div>
                </div>
              ))
            ) : (
              <h4 style={{ paddingLeft: '20px' }}>You don&#39;t have any favorites items yet!</h4>
            )}
          </div>
        </div>
      </div>
    </>
  );
};

export default FavoritesPage;
