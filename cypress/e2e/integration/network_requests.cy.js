/// <reference types="cypress" />

context('Network Requests', () => {
  beforeEach(() => {
    cy.visit('http://localhost:3000/');
  });

  it('gets all filters for home page', () => {
    // https://on.cypress.io/request
    cy.request('https://json-server-test-7c6787734c77.herokuapp.com/colors').should((response) => {
      expect(response.status).to.eq(200);
      // the server sometimes gets an extra comment posted from another machine
      // which gets returned as 1 extra object
      expect(response.body).to.have.property('length').and.be.oneOf([18, 19]);
      expect(response).to.have.property('headers');
      expect(response).to.have.property('duration');
    });
    cy.request('https://json-server-test-7c6787734c77.herokuapp.com/tags').should((response) => {
      expect(response.status).to.eq(200);
      // the server sometimes gets an extra comment posted from another machine
      // which gets returned as 1 extra object
      expect(response.body).to.have.property('length').and.be.oneOf([25, 26]);
      expect(response).to.have.property('headers');
      expect(response).to.have.property('duration');
    });
    cy.request('https://json-server-test-7c6787734c77.herokuapp.com/materials').should(
      (response) => {
        expect(response.status).to.eq(200);
        // the server sometimes gets an extra comment posted from another machine
        // which gets returned as 1 extra object
        expect(response.body).to.have.property('length').and.be.oneOf([13, 14]);
        expect(response).to.have.property('headers');
        expect(response).to.have.property('duration');
      }
    );
    cy.request('https://json-server-test-7c6787734c77.herokuapp.com/manufacturers').should(
      (response) => {
        expect(response.status).to.eq(200);
        // the server sometimes gets an extra comment posted from another machine
        // which gets returned as 1 extra object
        expect(response.body).to.have.property('length').and.be.oneOf([9, 10]);
        expect(response).to.have.property('headers');
        expect(response).to.have.property('duration');
      }
    );
  });
  it('gets first 20 items for home page', () => {
    // https://on.cypress.io/request
    cy.request(
      'https://json-server-test-7c6787734c77.herokuapp.com/vrscans?_limit=20&_page=1'
    ).should((response) => {
      expect(response.status).to.eq(200);
      // the server sometimes gets an extra comment posted from another machine
      // which gets returned as 1 extra object
      expect(response.body).to.have.property('length').and.be.oneOf([19, 20]);
      expect(response).to.have.property('headers');
      expect(response).to.have.property('duration');
    });
  });
});
