/// <reference types="cypress" />
function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

context('Search', () => {
  beforeEach(() => {
    cy.visit('http://localhost:3000/');
    sleep(5000);
  });

  it('should update search input value on change', () => {
    const searchQuery = 'cnv';

    cy.get('.form-control').type(searchQuery).should('have.value', searchQuery);
  });

  it('should call handleSearch function on form submission', () => {
    cy.get('.form-control').type('cnv');
    cy.get('.search-button').click();
  });

  it('should make the correct API request when a search value is typed', () => {
    const searchQuery = 'cnv';

    cy.intercept(
      'GET',
      `https://json-server-test-7c6787734c77.herokuapp.com/vrscans?_page=2&_limit=20&name_like=${searchQuery}`
    ).as('searchRequest');

    cy.get('.form-control').type(searchQuery);
    cy.wait('@searchRequest').then((interception) => {
      expect(interception.response.statusCode).to.equal(200);
    });
  });
});
