const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');
const ESLintPlugin = require('eslint-webpack-plugin');
const Dotenv = require('dotenv-webpack');

module.exports = {
  mode: 'development',
  entry: './src/index.js',
  output: {
    path: path.resolve(__dirname, 'public'),
    filename: 'index.js',
    clean: true
  },
  resolve: {
    extensions: ['.js', '.jsx']
  },
  devServer: {
    static: './public',
    hot: true,
    historyApiFallback: { index: '/', disableDotRule: true },
    port: 3000
  },
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: ['style-loader', 'css-loader', 'sass-loader']
      },
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader'
        }
      }
    ]
  },
  plugins: [
    new Dotenv(),
    new HtmlWebpackPlugin(
      {
        template: './public/index.html',
        inject: false
      },
      new ESLintPlugin()
    )
  ]
};
